import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const ul = document.querySelector("ul");

  const API_URL = `https://pokeapi.co/api/v2/pokemon?limit=10`;

  fetch('https://pokeapi.co/api/v2/pokemon?limit=10').then(r => r.json()).then(d => {
    d.results.forEach(pokemon => {
      const { name } = pokemon;
      const li = document.createElement('li');
      li.innerText = name;
      ul.appendChild(li);
    });
  })
});
